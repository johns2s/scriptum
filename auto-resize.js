function resize(event) {
    event.target.style.height = "auto";
    event.target.style.height = event.target.scrollHeight + "px";
  }

  var textareas = document.querySelectorAll('textarea[auto-resize]');

  for (var i in textareas) {
    var el = textareas.item(i);

    el.addEventListener('input', resize, false);
    el.style.resize = "none";
  }
